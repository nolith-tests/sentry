#!/bin/bash

URL="https://sentry.io/api/0/organizations/nolith-test/releases/"
auth="Authorization: Bearer $RELEASES_TOKEN"

version=$(git rev-parse HEAD)
http --check-status get "${URL}${version}" "$auth" || curl "$URL"  \
         -X POST \
         -H 'Authorization: Bearer {RELEASES_TOKEN}' \
         -H 'Content-Type: application/json' \
         -d '
 {
 "version": "$version",
 "refs": [{
 "repository":"tests-by-nolith / sentry",
 "commit":"$version"
 }],
 "projects":["alessio-caiazza"]
 }
'
